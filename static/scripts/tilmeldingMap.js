var map;
var geoCoder;
var markers=[];
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 56.1206, lng: 10.1464},
    zoom: 11
  });
  geoCoder = new google.maps.Geocoder();
}

function codeAddress(start, slut){
  deleteAllMarkers();
  geoCoder.geocode({'address':start},function(results,status){
    if(status=='OK'){
      addMarker(results[0].geometry.location,true);
      map.setCenter(results[0].geometry.location);
    }
  });
  geoCoder.geocode({'address':slut},function(results,status){
    if(status=='OK'){
      addMarker(results[0].geometry.location,false);
    }
  })
  showMarkers();
}

function setMapOnAll(map){
  for(var i =0; i<markers.length;i++){
    markers[i].setMap(map);
  }
}

function addMarker(position, isOpsamling){
  var marker = new google.maps.Marker({
    map:map,
    position: position
  });
  if(isOpsamling){
    marker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png')
  } else {
    marker.setIcon('http://maps.google.com/mapfiles/ms/icons/blue-dot.png')  
  }
  markers.push(marker);
}

function deleteAllMarkers(){
  setMapOnAll(null);
  markers=[];
}

function showMarkers(){
  setMapOnAll(map);
}