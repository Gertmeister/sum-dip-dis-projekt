var chai = require('chai');
var expect = require('chai').expect;
var should = chai.should();
var mongoose = require('mongoose');

var testBruger = require('./Models/Bruger.model.js');
var testTilmelding = require('./Models/Tilmelding.model.js');
var testOpsamling = require('./Models/Opsamling.model.js');

describe('bruger postnummer', function () {
    it('it should be invalid if postnr is 123', function (done) {
        var b = new testBruger({ postnr: '123' })

        b.validate(function (err) {
            expect(err.errors.postnr).to.exist;
            done();
        });
    });

    it('it should be invalid if postnr is 12345', function (done) {
        var b = new testBruger({ postnr: '12345' })

        b.validate(function (err) {
            expect(err.errors.postnr).to.exist;
            done();
        });
    });

    it('it should be invalid if postnr is empty', function (done) {
        var b = new testBruger({})

        b.validate(function (err) {
            expect(err.errors.postnr).to.exist;
            done();
        });
    });

    it('it should be valid when postnr is 1234', function (done) {
        var b = new testBruger({ postnr: '1234' })

        b.validate(function (err) {
            expect(err.errors.postnr).to.not.exist;
            done();
        });
    });
});

describe('bruger telefonnummer', function () {
    it('it should be invalid if tlfnr is 12345', function (done) {
        var b = new testBruger({ telefonnummer: '12345' })

        b.validate(function (err) {
            expect(err.errors.telefonnummer).to.exist;
            done();
        });
    });

    it('it should be invalid if telefonnummer is 123456789', function (done) {
        var b = new testBruger({ telefonnummer: '123456789' })

        b.validate(function (err) {
            expect(err.errors.telefonnummer).to.exist;
            done();
        });
    });

    it('it should be invalid if telefonnummer is empty', function (done) {
        var b = new testBruger({})

        b.validate(function (err) {
            expect(err.errors.telefonnummer).to.exist;
            done();
        });
    });

    it('it should be valid when telefonnummer is 12345678', function (done) {
        var b = new testBruger({ telefonnummer: '12345678' })

        b.validate(function (err) {
            expect(err.errors.telefonnummer).to.not.exist;
            done();
        });
    });
});

describe('bruger email', function () {
    it('should be invalid when email is empty', function (done) {
        var b = new testBruger({})

        b.validate(function (err) {
            expect(err.errors.email).to.exist;
            done();
        });
    });

    it('should be valid when email is test@test.com', function (done) {
        var b = new testBruger({ email: 'test@test.com' })

        b.validate(function (err) {
            expect(err.errors.email).to.not.exist;
            done();
        });
    });

    it('should be valid when email is test@students.eaaa.dk', function (done) {
        var b = testBruger({ email: 'test@students.eaaa.dk' })

        b.validate(function (err) {
            expect(err.errors.email).to.not.exist;
            done();
        });
    });
});

var testbruger1, testbruger2, testbruger3, testbruger4, testopsamling1, testopsamling2, testopsamling3,  testtilmelding1, testtilmelding2, testtilmelding3;

describe('Tilmelding model test#1:', () => {
    beforeEach((done) => {
        testbruger1 = new testBruger({
            fornavn: 'Fornavn',
            efternavn: 'Efternavn',
            adresse: 'Adresse',
            postnr: '1234',
            telefonnummer: '12345678',
            email: 'email@email.com',
            password: 'password'
        });

        testbruger1.save(() => {
            testbruger2 = new testBruger({
                fornavn: 'Foornavn',
                efternavn: 'Effternavn',
                adresse: 'Adressse',
                postnr: '1234',
                telefonnummer: '11111111',
                email: 'emaiil@email.com',
                password: 'password'
            });
            testbruger2.save(() => {
                testbruger3 = new testBruger({
                    fornavn: 'Fooornavn',
                    efternavn: 'Efffternavn',
                    adresse: 'Adresssse',
                    postnr: '1234',
                    telefonnummer: '22222222',
                    email: 'emaiiil@email.com',
                    password: 'password'
                });

                testbruger3.save(() => {
                    testopsamling1 = new testOpsamling({
                        startpunkt: 'startpunkt',
                        slutpunkt: 'slutpunkt',
                        tidspunkt: 'tidspunkt',
                        passagerMax: 3,
                        chauffoer: testbruger1.id,
                        passagerer: [testbruger2.id, testbruger3.id]
                    });
        
        
        
                    testopsamling1.save(() => {
                        testtilmelding1 = new testTilmelding({
                            brugerID: testbruger1.id,
                            opsamling: testopsamling1.id
                        })
                        done();
                    })
                })
            })
        })

    })

    describe('Tester oprettelse af tilmelding', () => {
        it('Skulle ikke oprette når chauffør er tilmelder', () => {

            testtilmelding1.save((err) => {
                should.exist(err);
            });
        });
    })


    afterEach((done) => {
        testtilmelding1.remove(() => {
            testopsamling1.remove(() => {
                testbruger3.remove(() => {
                    testbruger2.remove(() => {
                        testbruger1.remove(() => {
                            done();
                        });
                    })
                })
            });
        });
    });
});

describe('Tilmelding model test#2:', () => {
    beforeEach((done) => {
        testbruger1 = new testBruger({
            fornavn: 'Fornavn',
            efternavn: 'Efternavn',
            adresse: 'Adresse',
            postnr: '1234',
            telefonnummer: '12345678',
            email: 'email@email.com',
            password: 'password'
        });

        testbruger1.save(() => {
            testbruger2 = new testBruger({
                fornavn: 'Foornavn',
                efternavn: 'Effternavn',
                adresse: 'Adressse',
                postnr: '1234',
                telefonnummer: '11111111',
                email: 'emaiil@email.com',
                password: 'password'
            });
            testbruger2.save(() => {
                testbruger3 = new testBruger({
                    fornavn: 'Fooornavn',
                    efternavn: 'Efffternavn',
                    adresse: 'Adresssse',
                    postnr: '1234',
                    telefonnummer: '22222222',
                    email: 'emaiiil@email.com',
                    password: 'password'
                });

                testbruger3.save(() => {
                    testopsamling2 = new testOpsamling({
                        startpunkt: 'startpunkt',
                        slutpunkt: 'slutpunkt',
                        tidspunkt: 'tidspunkt',
                        passagerMax: 3,
                        chauffoer: testbruger1.id,
                        passagerer: [testbruger2.id, testbruger3.id]
                    });
        
        
        
                    testopsamling2.save(() => {
                        testtilmelding2 = new testTilmelding({
                            brugerID: testbruger2.id,
                            opsamling: testopsamling2.id
                        })
                        done();
                    })
                })
            })
        })

    })

    describe('Tester oprettelse af tilmelding', () => {
        it('Skulle ikke oprette når bruger allerede er passager', () => {

            testtilmelding2.save((err) => {
                should.exist(err);
            });
        });
    })
    afterEach((done) => {
        testtilmelding2.remove(() => {
            testopsamling2.remove(() => {
                testbruger3.remove(() => {
                    testbruger2.remove(() => {
                        testbruger1.remove(() => {
                            done();
                        });
                    })
                })
            });
        });
    });
});

describe('Tilmelding model test#3:', () => {
    beforeEach((done) => {
        testbruger1 = new testBruger({
            fornavn: 'Fornavn',
            efternavn: 'Efternavn',
            adresse: 'Adresse',
            postnr: '1234',
            telefonnummer: '12345678',
            email: 'email@email.com',
            password: 'password'
        });

        testbruger1.save(() => {
            testbruger2 = new testBruger({
                fornavn: 'Foornavn',
                efternavn: 'Effternavn',
                adresse: 'Adressse',
                postnr: '1234',
                telefonnummer: '11111111',
                email: 'emaiil@email.com',
                password: 'password'
            });
            testbruger2.save(() => {
                testbruger3 = new testBruger({
                    fornavn: 'Fooornavn',
                    efternavn: 'Efffternavn',
                    adresse: 'Adresssse',
                    postnr: '1234',
                    telefonnummer: '22222222',
                    email: 'emaiiil@email.com',
                    password: 'password'
                });

                testbruger3.save(() => {
                    testbruger4 = new testBruger({
                        fornavn: 'foooornavn',
                        efternavn: 'Effffternavn',
                        adresse: 'adressssse',
                        postnr: '1234',
                        telefonnummer: '33333333',
                        email: 'emaiiiil@email.com',
                        password: 'password'
                    });

                    testbruger4.save(() => {
                        testopsamling3 = new testOpsamling({
                            startpunkt: 'startpunkt',
                            slutpunkt: 'slutpunkt',
                            tidspunkt: 'tidspunkt',
                            passagerMax: 2,
                            chauffoer: testbruger1.id,
                            passagerer: [testbruger2.id, testbruger3.id]
                        });
            
            
            
                        testopsamling3.save(() => {
                            testtilmelding3 = new testTilmelding({
                                brugerID: testbruger4.id,
                                opsamling: testopsamling3.id
                            })
            
                            done();
                        })
                    })
                })
            })
        })

    })

    describe('Tester oprettelse af tilmelding', () => {
        it('Skulle ikke oprette når opsamling er på maks passagerer', () => {

            testtilmelding3.save((err) => {
                should.exist(err);
            });
        });
    })
    
    afterEach((done) => {
        testtilmelding3.remove(() => {
            testopsamling3.remove(() => {
                testbruger4.remove(() => {
                    testbruger3.remove(() => {
                        testbruger2.remove(() => {
                            testbruger1.remove(() => {
                                done();
                            });
                        })
                    })
                })
            });
        });
    });
    
});