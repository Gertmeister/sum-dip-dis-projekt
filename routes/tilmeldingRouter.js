var express = require('express'),
    router = express.Router(),
    Tilmelding = require('../Models/Tilmelding.model'),
    Opsamling = require('../Models/Opsamling.model'),
    ObjectId = require('mongodb').ObjectId,
    sortering = require('../modules/sort'),
    server = require('../server');


router.get('/tilmelding', function (req, res) {
    if (res.locals.authenticated) {
        Opsamling.find({})
            .populate({ path: 'passagerer', select: 'fornavn-_id' })
            .populate({ path: 'chauffoer', select: 'fornavn-_id' })
            .exec(function (err, doc) {
                if (err) {
                    res.render('error', { err: err });
                } else {
                    //console.log(doc)
                    doc.sort(sortering.sortOpsamlinger('tidspunkt'));
                    res.render('tilmelding', { opsamlinger: doc });
                }
            });
    } else {
        res.redirect('/')
    }
});

router.post('/tilmelding/:id', function (req, res) {
    var tilmelding = new Tilmelding();
    tilmelding.brugerID = req.session.loggedIn;
    tilmelding.opsamling = req.params.id;
    tilmelding.save(function (err, tilmeld) {
        if (!err) {
            Opsamling.findByIdAndUpdate({ _id: req.params.id },
                {
                    $push: { passagerer: req.session.loggedIn }
                }, function (err, doc) {
                    if (err) {
                        res.render('error', { err: err })
                    } else {
                        res.redirect('/');
                    }
                });
        } else {
            res.render('error', { err: err })
        }

    });
});

module.exports = router;