const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const testBrugerSchema = new Schema({
	fornavn			: { 
		type: String, 
		required:true,
		trim: true
	},
	efternavn		: { 
		type: String, 
		required:true,
		trim: true
	},
	adresse			: { 
		type: String, 
		required:true,
		trim: true
	},
	postnr			: { 
		type: String, 
		required:true,
		trim: true,
		validate: [function(postnr) {
			return /^\d{4}$/.test(postnr)
		}, 'Ugyldigt PostNr']
	},
	telefonnummer	: { 
		type: String, 
		required:true,
		trim: true,
		validate: [function(telefonnummer) {
			return /^\d{8}$/.test(telefonnummer)
		}, 'Ugyldigt Telefonnummer']
	},
	email			: { 
		type: String, 
		lowercase: true,
		trim: true,
		required: [true, "Email is required"],
		unique: true,
		validate: [function(email) {
			return  	
			/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)
		}, 'Ugyldig Emailadresse']
	},
	password		: { 
		type: String, 
		required:true,
		trim: true,

	}
});

var testBruger = mongoose.model('testBruger', testBrugerSchema);

testBrugerSchema.path('email').validate(function(value, respond) {

	testBruger.findOne({email: value}, function(err, doc) {
		if(err) {
			respond(false, 'Unable to check due to database error');
		} else if (doc) {
			respond(false, 'Email already exists');
		} else {
			respond(true)
		}
	})
});

module.exports = testBruger;
