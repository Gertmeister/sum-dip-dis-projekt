
var express = require('express');
var router = express.Router();
var Bruger = require('../Models/Bruger.model.js');
var ObjectId = require('mongodb').ObjectId;
var server = require('../server.js');





// viser loginsiden
router.get('/login', function (req, res) {
    res.render('login')
})
// viser opret bruger siden
router.get('/opretBruger', function (req, res) {
    res.render('opretBruger')
});

// viser login siden med email
router.get('/login/:signupEmail', function (req, res) {
    res.render('login', { signupEmail: req.params.signupEmail })
})

router.post('/login', function (req, res, next) {
    Bruger.findOne({ email: req.body.bruger.email, password: req.body.bruger.password },
        function (err, doc) {
            if (err) {
                return res.render('error',{err: err});
            }
            if (!doc) {
                return res.render('login', {signupEmail: req.body.bruger.email, err: 'err'} )
            }
            req.session.loggedIn = doc._id.toString();
            res.redirect('/');
        });
});

// et post request, hvor en ny bruger kan oprettes
router.post('/opretBruger', function (req, res) {
    var nyBruger = new Bruger()

    nyBruger.fornavn = req.body.bruger.fornavn
    nyBruger.efternavn = req.body.bruger.efternavn
    nyBruger.adresse = req.body.bruger.adresse
    nyBruger.postnr = req.body.bruger.postnr
    nyBruger.telefonnummer = req.body.bruger.telefonnummer
    nyBruger.email = req.body.bruger.email
    nyBruger.password = req.body.bruger.password

    nyBruger.save(function (err, bruger) {
        if (err) {
            res.status(400);
            res.render('opretBruger', { email: nyBruger.email, fornavn: nyBruger.fornavn, efternavn: nyBruger.efternavn, adresse: nyBruger.adresse, postnr: nyBruger.postnr, telefonnummer: nyBruger.telefonnummer, errors: err.errors })
        } else {
            res.status(200);
            res.render('login', { signupEmail: bruger.email })

        }
    })
})

router.get('/opdaterBruger/', function (req, res) {
    if(res.locals.authenticated) {
        res.render('opdaterBruger')        
    } else {
        res.redirect('/')
    }
})




router.post('/opdaterBruger/', function (req, res) {
    var fornavn = req.body.bruger.fornavn
    var efternavn = req.body.bruger.efternavn
    var adresse = req.body.bruger.adresse
    var postnr = req.body.bruger.postnr
    var telefonnummer = req.body.bruger.telefonnummer
    var email = req.body.bruger.email
    var password = req.body.bruger.password

    if (req.session.loggedIn)
        console.log(fornavn)
    Bruger.findById({ _id: res.locals.me._id }, function (err, doc) {
        if (err) {
            res.render('error',{err: err})
        }
        else {
            if (req.session.loggedIn == req.params.id) {

                doc.fornavn = fornavn;
                doc.efternavn = efternavn;
                doc.adresse = adresse;
                doc.postnr = postnr;
                doc.telefonnummer = telefonnummer;
                doc.email = email;
                doc.save(function(err, doc) {
                    if(!err) {
                        res.redirect('/')
                    } else {res.render('opdaterBruger', { 
                        email: email, fornavn: fornavn, efternavn: efternavn, adresse: adresse, postnr: postnr, telefonnummer: telefonnummer, errors: err.errors })
                    }
                });


            }
        }
    })
});


router.get('/opdaterPassword/', function (req, res) {
    if(res.locals.authenticated) {
        res.render('opdaterPassword');        
    } else {
        res.redirect('/')
    }
})


router.post('/opdaterPassword/', function (req, res, err) {
    if(res.locals.me.password==req.body.bruger.password){
        if(req.body.bruger.password==req.body.bruger.newPass||req.body.bruger.newPass!=req.body.bruger.confPass){
            res.send(err.toString());
        } else {
            Bruger.findByIdAndUpdate({_id:req.session.loggedIn},{password:req.body.bruger.confPass},function(err1,doc){
                if(err1){
                    res.render('error',{err: err})
                } else {
                    res.redirect('/');
                }
            })
        }
    } else {
        res.render('error',{err: "Forkert password"})
    }
})




// log ud
router.get('/logout', function (req, res) {
    req.session.loggedIn = null;
    res.redirect('/');
});

module.exports = router;