var schedule = require('node-schedule'),
    Opsamling = require('../Models/Opsamling.model.js'),
    Tilmelding = require('../Models/Tilmelding.model.js');

exports.tilmeldingCleanup = function() {
    var tilRule = new schedule.RecurrenceRule();
    tilRule.hour = 1
    tilRule.minute = 1
    
    var j = schedule.scheduleJob(tilRule, function() {
        var dato = new Date();
    
        Tilmelding.find({}).exec(function(err, tilmeldinger) {
            if (!err) {
                tilmeldinger.forEach(function(tilmelding) {
                    var o = tilmelding.opsamling
    
                    Opsamling.findOne({_id: o}, function(err, result) {
                        if(!err) {
                            var oDato = new Date(result.tidspunkt);
                            if(oDato < dato) {
                                Tilmelding.findByIdAndRemove({ _id: tilmelding._id})
                            }
                        } else {
                            console.log(err)
                        }
                    })
                })
            } else {
                console.log(err)
            }
        })
    })
}

exports.opsamlingCleanup = function() {
    var opRule = new schedule.RecurrenceRule();
    opRule.hour = 1
    opRule.minute = 6
    
    var oprydOpsamling = schedule.scheduleJob(opRule, function() {
        var dato = new Date();
    
        Opsamling.find({}).exec(function(err, opsamlinger) {
            if(!err) {
                opsamlinger.forEach(function(opsamling) {
                    var o = opsamling;
                    var oDato = new Date(o.tidspunkt);
                    if(oDato < dato) {
                        Opsamling.findByIdAndRemove({ _id: o._id})
                    }
                })
            } else {
                console.log(err)
            }
        })
    })
}