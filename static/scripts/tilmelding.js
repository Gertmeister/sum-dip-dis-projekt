$(document).ready(function(){
    $("li").each(function(index){
        $(this).find("#info").hide();
    });

    $('li').on("click",function(){
        if($(this).find("#info").is(':hidden')){
            var e = this;
            $("li").each(function(index) {
                $(this).find("#info").hide();
            })
            $(e).find('#info').show();
            var start = $(this).find('#start').html().replace(/Startpunkt: /,"");
            var slut = $(this).find('#slut').html().replace(/Slutpunkt: /,"");
            var tid = $(this).find('#tid').html().replace(/Tidspunkt: /,"");
            var date = new Date(tid);
            $(this).find('#tid').text("Tidspunkt: "+date.toLocaleTimeString("en-GB")+" , "+date.toLocaleDateString());
            codeAddress(start,slut);
        } else {
            $(this).find('#info').hide();
            deleteAllMarkers();
        }
    });
    $('.anchor').on("click",function(event){
        event.preventDefault();
    })
});