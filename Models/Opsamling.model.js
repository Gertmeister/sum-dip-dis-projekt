const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Bruger = require('./Bruger.model');

const OpsamlingSchema = new Schema ({
	startpunkt	: {
		type: String,
		required: true,
		trim: true
	},
	slutpunkt	: {
		type: String,
		required: true,
		trim: true
	},
	tidspunkt	: {
		type: String,
		required: true,
		trim: true
	},
	passagerMax	: {
		type: Number,
		required: true,
		trim: true
	},
	chauffoer	: {
		type: Schema.Types.ObjectId, 
		ref: 'Bruger',
		required: true
	},
	passagerer	: [{
		type: Schema.Types.ObjectId, 
		ref: 'Bruger'
	}],
	status	: {
		type: Boolean,
		required: true,
		default: true
	}
});

var Opsamling = mongoose.model('Opsamling', OpsamlingSchema);

module.exports = Opsamling;