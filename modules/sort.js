exports.sortTilmeldingerByDate = function() {
    return function(a, b) {
        var _a = a['opsamling']
        var _b = b['opsamling']
        if(_a.tidspunkt > _b.tidspunkt) {
            return 1;
        } else if (_a.tidspunkt< _b.tidspunkt) {
            return -1;
        }
        return 0;
    }
}

exports.sortOpsamlinger = function(attr) {
    return function(a, b) {
        if(a[attr] > b[attr]) {
            return 1;
        } else if (a[attr] < b[attr]) {
            return -1;
        }
        return 0;
    }
}