const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const testBruger = require('./Bruger.model');

const OpsamlingSchema = new Schema ({
	startpunkt	: {
		type: String,
		required: true,
		trim: true
	},
	slutpunkt	: {
		type: String,
		required: true,
		trim: true
	},
	tidspunkt	: {
		type: String,
		required: true,
		trim: true
	},
	passagerMax	: {
		type: Number,
		required: true,
		trim: true
	},
	chauffoer	: {
		type: Schema.Types.ObjectId, 
		ref: 'Bruger',
		required: true
	},
	passagerer	: [{
		type: Schema.Types.ObjectId, 
		ref: 'Bruger'
	}]
});

var testOpsamling = mongoose.model('testOpsamling', OpsamlingSchema);

/*
OpsamlingSchema.path('chauffoer').validate(function(id, respond) {
	testBruger.findOne({_id: id}, function(err, doc) {
		if(err) {
			respond(false, 'Unable to check due to databse error')
		} else if (!doc) {
			respond(false, 'User does not exist in the database')
		} else {
			respond(true)
		}
	})
})

OpsamlingSchema.path('passagerer').validate(function(id, respond) {
	testBruger.findOne({_id: id}, function(err, doc) {
		if(err) {
			respond(false, 'Unable to check due to databse error')
		} else if(!doc) {
			respond(false, 'User does not exist in the databse')	
		} else {
			respond(true)	
		}
	})
})
*/

module.exports = testOpsamling;