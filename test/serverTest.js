
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server');
var should = chai.should();
var chaiDom = require('chai-dom');
var expect = require('chai').expect;
var supertest = require('supertest')
chai.use(chaiHttp);
var testBruger = {bruger: {email:'test@tb.dk',password:'123'}}
var authBruger = supertest.agent(server)



before(function(done) {
    authBruger
    .post('/login')
    .set('content-type', 'application/x-www-form-urlencoded')
    .send(testBruger)
    .end(function(err, response){
      expect(response.statusCode).to.equal(302);
      expect('Location', '/');
      done();
    })
})


describe('opretopsamling test', function () {
    it('authenticated get request', function (done) {
        authBruger.get('/opretopsamling').expect(200,done)
        })
    it('POST /opretopsamling returns ok', function (done) {
        authBruger.post('/opretopsamling').set('content-type', 'application/x-www-form-urlencoded')
                    .send({ opsamling: { passagerMax: 4,
                    tidspunkt: "2017-12-27T08:00",
                    slutpunkt: "Vestervang 3, 8000 Aarhus C, Denmark",
                    startpunkt: "Bavnegårdsvej 147, 8361 Hasselager, Denmark" } })
                    .expect('Location', '/')
                    .expect(302, done);
            });
});

describe('/GET', function () {
    it('should give front page GET', function (done) {
        chai.request(server)
            .get('/')
            .end(function (err, res) {
                res.should.have.status(200);
                res.should.be.html;
                done();
            });
    });
    it('should give /opretBruger GET', function (done) {
        chai.request(server)
            .get('/opretBruger')
            .end(function (err, res) {
                res.should.have.status(200);
                res.should.be.html;
                done();
            });
    });
    it('should give /login GET', function (done) {
        chai.request(server)
            .get('/login')
            .end(function (err, res) {
                res.should.have.status(200);
                res.should.be.html;
                done();
            });
    });
    it('should give /login/:signupEmail GET', function (done) {
        chai.request(server)
            .get('/login/:signupEmail')
            .end(function (err, res) {
                res.should.have.status(200);
                res.should.be.html;
                done();
            });
    });

})


/**
 * Testen fejler, hvis den køres flere gange, fordi email er unique og brugeren
 * ikke kan oprettes flere gange
 */
describe('/POST', function () {
    it('skal oprette en bruger på POST', function (done) {
        chai.request(server)
            .post('/opretBruger')
            .send({ 'bruger': { 'email': 'test@tb.dk', 'fornavn': 'java', 'efternavn': 'Script', 'password': '123', 'telefonnummer': '11111111', 'adresse': 'skolevej', 'postnr': '1111' } })
            .end(function (err, res) {
                res.should.have.status(200);
                res.should.be.html;
                done();
            });
    })
    /**
     * Tester om email validering er korrekt
     */
    it('skal oprette en bruger på POST', function (done) {
        chai.request(server)
            .post('/opretBruger')
            .send({ 'bruger': { 'email': 'a@a.com', 'fornavn': 'java', 'efternavn': 'Script', 'password': '123', 'telefonnummer': 'fgd', 'adresse': 'skolevej', 'postnr': '1111' } })
            .end(function (err, res) {
                res.should.have.status(400);
                res.should.be.html;
                done();
            });
    })



});



describe('Tilmelding', function () {
    describe('GET/', function () {
        it('Server should return a view page with and unordered list of opsamlinger', function (done) {
            chai.request(server)
                .get('/tilmelding')
                .end(function (err, res) {
                    res.should.have.status(200);
                    res.should.be.html;
                    done();
                });
        });
    });
    describe('POST/', function () {
        it('Server should update the particular opsamling by adding the current user as a passenger and return the updated object', function (done) {
            chai.request(server)
                .post('/tilmelding/id=5a12d54ba46bea31651de446')
                .end(function (err, res) {
                    res.should.have.status(200);
                    res.should.be.html;
                    done();
                });
        });
    });
});

// Test 'Opdatér password' GET
describe('test af /opdaterPassword', function () {
    it('GET /opdaterPassword returns OK', function () {
        chai.request(server).get('opdaterPassword').end(function (err, res) {
            chai.expect(res).to.have.status(200);
            chai.assert.containsAllDeepKeys(res, ['password', 'newPass', 'confPass']);
            done();
        })
    });
    // Test 'Opdatér password' POST
    it('POST /opdaterPassword returns OK', function () {
        var agent = chai.request.agent(server);
        agent
            .post('/login').set('content-type', 'application/x-www-form-urlencoded')
            .send({ bruger: { email: 'test@test', password: '321' } })
            .then(function (res) {
                agent.post('/opdaterPassword').set('content-type', 'application/x-www-form-urlencoded')
                    .send({ password: { password: '321', newPass: '123,', confPass: '123' } })
                    .then(function (err, res2) {
                        chai.expect(res2).to.have.status(200);
                        done();
                    });
            });
    });
});