var express = require('express');
var router = express.Router();
var Opsamling = require('../Models/Opsamling.model.js');
var ObjectId = require('mongodb').ObjectId;
var server = require('../server.js');

router.get('/opretopsamling', function(req, res) {
    if(res.locals.authenticated) {
        res.render('opretopsamling')        
    } else {
        res.redirect('/')
    }
})

router.post('/opretopsamling', function(req, res, next) {
    var nyOpsamling = new Opsamling()

    nyOpsamling.startpunkt = req.body.opsamling.startpunkt
    nyOpsamling.slutpunkt = req.body.opsamling.slutpunkt
    nyOpsamling.tidspunkt = req.body.opsamling.tidspunkt
    nyOpsamling.passagerMax = req.body.opsamling.passagerMax
    nyOpsamling.chauffoer = res.locals.me._id
    nyOpsamling.save(function(err, doc) {
        if(err) {
            res.render('opretopsamling',{startP:req.body.opsamling.startpunkt,slutP:req.body.opsamling.slutpunkt,tidP:req.body.opsamling.tidspunkt,pMax: req.body.opsamling.passagerMax,errors:err.errors})
        } else {
            res.redirect('/')
        }
    })
})

module.exports = router