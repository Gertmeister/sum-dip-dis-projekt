const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Bruger = require('./Bruger.model');
const Opsamling = require('./Opsamling.model');

const TilmeldingSchema = new Schema ({
	brugerID	: {
		type: Schema.Types.ObjectId, 
		ref: 'Bruger', 
		required:true
	},
	opsamling	: {
		type: Schema.Types.ObjectId, 
		ref: 'Opsamling', 
		required:true
	}
});

var Tilmelding = mongoose.model('Tilmelding', TilmeldingSchema);

TilmeldingSchema.path('opsamling').validate(function(id, respond) {
	var self = this
	var selfid = self.brugerID;
	Opsamling.findOne({_id: id}, function(err, doc) {
		if(err) {
			respond(false, 'Fejl - kan ikke søge i databasen')
		} else if (!doc) {
			respond(false, 'Kan ikke finde Opsamling i databasen')
		} else {
			var opsamling = doc;
			var chauffid = opsamling.chauffoer
			if(compare(doc.chauffoer.id,self.brugerID.id)) {
				respond(false, 'Du er chauffør for Opsamlingen')
			} else {
				respond(true);
			}
		}
	})
})

TilmeldingSchema.path('opsamling').validate(function(id, respond) {
	var self = this;
	Opsamling.findOne({_id: id}, function(err, doc) {
		if(err) {
			respond(false, 'Fejl - kan ikke søge i databasen')
		} else if (!doc) {
			respond(false, 'Kan ikke finde Opsamling i databasen')
		} else {
			var opsamling = doc;
			if(opsamling.passagerer.indexOf(self.brugerID) > -1) {
				respond(false, 'Du er allerede tilmeldt denne Opsamling')
			} else {
				respond(true)
			}
		}
	})
})

TilmeldingSchema.path('opsamling').validate(function(id, respond) {
	var self = this;
	Opsamling.findOne({_id: id}, function(err, doc) {
		if(err) {
			respond(false, 'Fejl - kan ikke søge i databasen')
		} else if (!doc) {
			respond(false, 'Kan ikke finde Opsamling i databsen')	
		} else {
			var opsamling = doc;
			if(opsamling.passagerer.length == opsamling.passagerMax) {
				respond(false, 'Opsamlingen er fyldt')
			} else {
				respond(true)
			}
		}
	})
})

var compare = function(opt1, opt2) {
	var flag = true
	for (var i = 0; i<10;i++) {
		if(opt1[i]!=opt2[i]) {
			flag = false
		}
	}
	return flag
}

module.exports = Tilmelding;