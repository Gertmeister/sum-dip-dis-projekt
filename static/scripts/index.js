$(document).ready(function(){
    $("#tilmeldinger > *").each(function(index){
        $(".cancelled").css("background","linear-gradient(to right, #ffffff 0%, #ff0000 100%)");
    });
    $('.tid').each(function(index){
        var tid = $(this).html().replace(/Tidspunkt: /,"");
        var date = new Date(tid);
        $(this).text("Tidspunkt: "+date.toLocaleTimeString("en-GB")+" : "+date.toLocaleDateString());
    });
    $(".aflys").each(function(index){
        $(this).css("font-weight","bold")
        $(this).css("color","red")
    })
    if($("#opsamlinger > tbody").children().length <=1){
        console.log($("#opsamlinger").children().length);
        $("#opsamlinger").html("<h4>Du har endnu ikke oprettet nogle opsamlinger</h4>");
    }
    if($("#tilmeldinger > tbody").children().length <=1){
        console.log($("#tilmeldinger > tbody").children().length);
        $("#tilmeldinger").html("<h4>Du har endnu ikke tilmeldt dig nogle opsamlinger</h4>");
    }
    $('.startP').each(function(index){
        var txt = $(this).html().replace(/, Denmark|, Danmark/,"");
        $(this).text(txt);
    });
    $('.slutP').each(function(index){
        var txt = $(this).html().replace(/, Denmark|, Danmark/,"");
        $(this).text(txt);
    });
});