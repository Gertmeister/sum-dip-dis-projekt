'use strict';

var PORT = process.env.PORT||3000;

var express=require('express'),
    bodyParser=require('body-parser'),
    cookieSession=require('cookie-session'),
    cookieParser=require('cookie-parser'),
    mongoClient=require('mongodb').MongoClient,
    ObjectId=require('mongodb').ObjectId,
    morgan=require('morgan'),
    mongoose=require('mongoose'),
    Bruger = require('./Models/Bruger.model.js'),
    Opsamling = require('./Models/Opsamling.model.js'),
    Tilmelding = require('./Models/Tilmelding.model.js'),
    brugerR = require('./routes/brugerRouter'),
    opsamlingR = require('./routes/opsamlingRouter.js'),
    tilmeldingR = require('./routes/tilmeldingRouter.js'),
    sortering = require('./modules/sort.js'),
    dbcleanup = require('./modules/dbcleanup'),
    mongoUrl = require('./config/mongoConfig'),
    cookieOpt = require('./config/cookieConfig')
    

var server = express();
mongoose.Promise = global.Promise;

mongoose.connect(mongoUrl,{
    useMongoClient: true
});

var urlparser = bodyParser.urlencoded({extended:true});
var jsonparser = bodyParser.json();

server.use(urlparser)
server.use(jsonparser)
server.use(express.static('static'));
server.use(cookieParser());
server.use(morgan('tiny'));
server.use(cookieSession(cookieOpt));

server.set('view engine', 'pug');

// authentication
server.use(function (req, res, next) {
    if (req.session.loggedIn) {
        res.locals.authenticated = true;
        Bruger.findOne({ '_id': new ObjectId(req.session.loggedIn) },
            function (err, doc) {
                if (err) {
                    return next(err);
                }
                res.locals.me = doc;
                next();
            })
    } else {
        res.locals.authenticated = false;
        next();
    }
})

//viser forsiden
server.get('/', function (req, res) {
    if (res.locals.authenticated) {
        var mineOpsamlinger
        var mineTilmeldinger
        var mig = res.locals.me._id
        Opsamling.find({ 'chauffoer': mig })
            .exec(
            function (err, opsamlinger) {
                if (!err) {
                    mineOpsamlinger = opsamlinger
                    mineOpsamlinger.sort(sortering.sortOpsamlinger("tidspunkt"))
                }
            }).then(function () {
                Tilmelding.find({ 'brugerID': mig })
                    .populate({ path: 'opsamling', populate: { path: 'chauffoer', select: 'fornavn' } }).exec(function (err, tilmeldinger) {
                        if (!err) {
                            mineTilmeldinger = tilmeldinger
                            mineTilmeldinger.sort(sortering.sortTilmeldingerByDate());
                        } else {
                            res.render('index')  //, {authenticated: false}        
                        }
                    }).then(function () {
                        res.render('index', { tilmeld: mineTilmeldinger, opsaml: mineOpsamlinger })
                    })
            })

    }
    else {
        res.redirect('/login')
    }
})

server.post('/', function(req,res) {
    Opsamling.findByIdAndUpdate({_id: req.body.button},{status: false}, function(err,doc){
    res.redirect('/')
    })
})

server.use('/', opsamlingR)
server.use('/', brugerR)
server.use('/', tilmeldingR)



//DATABASE CLEANUP--------------------------------------------------------
dbcleanup.tilmeldingCleanup();
dbcleanup.opsamlingCleanup();

//------------------------------------------------------------------------

server.listen(PORT);
console.log('Server listening on '+PORT);
module.exports = server