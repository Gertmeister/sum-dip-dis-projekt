var cookieOptions = {
    name: 'session',    
    keys: ['ss-key'],
    maxAge: 1*60*60*1000 //1 hour
}

module.exports = cookieOptions