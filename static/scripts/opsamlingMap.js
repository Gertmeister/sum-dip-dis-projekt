var map;
  // This example adds a search box to a map, using the Google Place Autocomplete
      // feature. People can enter geographical searches. The search box will return a
      // pick list containing a mix of places and predicted search terms.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
      var startPmarker
      var destinationMarker
      var tempMarker
      var infowindow
      var geocoder
      var markers

      function initAutocomplete() {
        var iwContent = '<div id="form">'+
                        '<p><button onclick="saveMarkerPos(true)">Opsamling</button></li></ul></div>'+
                        '<p><button onclick="saveMarkerPos(false)">Destination</button></li></ul></div>'
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 56.1206, lng: 10.1464},
            zoom: 12,
            mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        markers = [];

        geocoder = new google.maps.Geocoder;
        infowindow = new google.maps.InfoWindow({
            content: iwContent
          });
        
          google.maps.event.addListener(map, 'click', function(event) {
            marker = new google.maps.Marker({
              position: event.latLng,
              map: map
            });
            if (tempMarker) {
              tempMarker.setMap(null)
              tempMarker = null
            }
            tempMarker = marker

            google.maps.event.addListener(marker, 'click', function() {
              infowindow.open(map, marker);
            });
          });
  
    
  

        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place, and add listener
            var marker2 =new google.maps.Marker({
                map: map,
                icon: icon,
                title: place.name,
                position: place.geometry.location
              });
            
              google.maps.event.addListener(marker2, 'click', function() {
                infowindow.open(map, marker2);
              });
            
            markers.push(marker2);
            
            

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });

      }

      function saveMarkerPos(flag) {
        var latlng = {lat: infowindow.position.lat(), lng: infowindow.position.lng()}

        geocoder.geocode({'location': latlng}, function(results, status) {
          if(status === 'OK') {
            if(results[0]) {
              var address = results[0].formatted_address;
              var prettyAddress = results[0].address_components[1].short_name + " " + results[0].address_components[0].short_name +
              ", " + (results[0].address_components.length>5 ? results[0].address_components[5].short_name : "") + 
              " " + results[0].address_components[2].short_name
              var clickedMarker = infowindow.anchor
              if(clickedMarker===tempMarker) {
                tempMarker=null
              } else {
                var i = markers.indexOf(clickedMarker)
                markers.splice(i,1)
              }
              if(flag) {
                $('#oForm input[id=startPos]').val(address)
                $('#startPosLa').text(prettyAddress)
                if(startPmarker) {
                  startPmarker.setMap(null)
                }
                startPmarker = clickedMarker
                startPmarker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png')
              } else if (!flag) {
                $('#oForm input[id=slutPos]').val(address)
                $('#slutPosLa').text(prettyAddress)
                if(destinationMarker) {
                  destinationMarker.setMap(null)
                }
                destinationMarker = clickedMarker
                destinationMarker.setIcon('http://maps.google.com/mapfiles/ms/icons/blue-dot.png')
              }
            } else {
              window.alert('No results found');
            }
          } else {
            window.alert('Geocoder failed due to: ' + status);
          }
          infowindow.close();
        })
      }